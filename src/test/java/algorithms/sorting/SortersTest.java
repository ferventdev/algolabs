package algorithms.sorting;

import algorithms.sorting.impl.*;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class SortersTest {

    private static final int RANDOM_ARRAY_LENGTH = 1000;
    private static final int RANDOM_ITERATIONS_NUMBER = 100;

    private final Integer[] worst_case_array
            = { Integer.MAX_VALUE, 100, 80, 60, 40, 20, 0, -20, -40, -60, -80, -100, Integer.MIN_VALUE };
    private final Integer[] best_case_array
            = { Integer.MIN_VALUE, -100, -80, -60, -40, -20, 0, 20, 40, 60, 80, 100, Integer.MAX_VALUE };
    private final Random random = new Random();


    private static <T extends Comparable<? super T>> boolean isSorted(T[] a) {
        for (int i = 1; i < a.length; i++) {
            if (a[i - 1].compareTo(a[i]) > 0) return false;
        }
        return true;
    }

    private Integer[] getNewRandomArray() {
        Integer[] result = new Integer[RANDOM_ARRAY_LENGTH];
        for (int i = 0; i < result.length; i++) {
            result[i] = random.nextInt(Integer.MAX_VALUE);
        }
        return result;
    }

    private void sorterTest(Sorter sorter) {
        assertThrows(IllegalArgumentException.class, () -> sorter.sort(null));
        assertThrows(IllegalArgumentException.class, () -> sorter.sort(new Integer[1], null));

        Integer[] data = worst_case_array;
        sorter.sort(data);
        assertTrue(isSorted(data));

        data = best_case_array;
        sorter.sort(data);
        assertTrue(isSorted(data));

        for (int i = 0; i < RANDOM_ITERATIONS_NUMBER; i++) {
            data = getNewRandomArray();
            sorter.sort(data);
            assertTrue(isSorted(data));
        }
    }

    @Test
    void bubbleSortTest() {
        sorterTest(new BubbleSorter());
    }

    @Test
    void insertionSortTest() {
        sorterTest(new InsertionSorter());
    }

    @Test
    void selectionSortTest() {
        sorterTest(new SelectionSorter());
    }

    @Test
    void recursiveMergeSortTest() {
        sorterTest(new RecursiveMergeSorter());
    }

    @Test
    void iterativeMergeSortTest() {
        sorterTest(new IterativeMergeSorter());
    }

    @Test
    void quickSortTest() {
        sorterTest(new QuickSorter());
    }

    @Test
    void quickWithInsertionSortTest() {
        sorterTest(new QuickWithInsertionSorter());
    }

    @Test
    void quick3WaySortTest() {
        sorterTest(new Quick3WaySorter());
    }

    @Test
    void heapSortTest() {
        sorterTest(new HeapSorter());
    }
}