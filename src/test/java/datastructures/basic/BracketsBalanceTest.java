package datastructures.basic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BracketsBalanceTest {
    @Test
    void isBalanced() {

        assertTrue(BracketsBalance.isBalanced("aa(bb)cc"));
        assertTrue(BracketsBalance.isBalanced("qq[ww]rr"));
        assertTrue(BracketsBalance.isBalanced("ss{dd}ff"));
        assertTrue(BracketsBalance.isBalanced("z(x[c{vb}n]m)k"));
        assertTrue(BracketsBalance.isBalanced("z(x)r([c]pp[{v((j))b}n]m)k"));
        assertTrue(BracketsBalance.isBalanced("()[]{}(([[{{([])}}]])){}[](){}"));

        assertFalse(BracketsBalance.isBalanced("y)u"));
        assertFalse(BracketsBalance.isBalanced("y(rt)u}"));
        assertFalse(BracketsBalance.isBalanced("a[ui]v)"));
        assertFalse(BracketsBalance.isBalanced("aq(er"));
        assertFalse(BracketsBalance.isBalanced("a(sd)f[g"));
        assertFalse(BracketsBalance.isBalanced("z(xc)v[bn]n{m"));
        assertFalse(BracketsBalance.isBalanced("({)"));
        assertFalse(BracketsBalance.isBalanced("[(]"));
        assertFalse(BracketsBalance.isBalanced("{[}]"));

    }

}