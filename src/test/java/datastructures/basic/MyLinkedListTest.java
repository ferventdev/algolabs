package datastructures.basic;

import datastructures.basic.impl.MyLinkedList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyLinkedListTest {

    private String[] tokens = { "aa", "bb", "cc", "dd", "ee", "ff", "gg" };
    private MyList<String> list;


    @BeforeEach
    void setUp() {
        list = new MyLinkedList<>(tokens);
    }


    @Test
    void emptyListTest() {
        MyList<String> emptyList = new MyLinkedList<>();

        assertThat(emptyList.size(), is(0));
        assertTrue(emptyList.isEmpty());
    }

    @Test
    void addAndRemove() {
        int initialSize = list.size();
        list.add(0, "first");
        list.add(list.size(), "last");
        list.add(list.size() / 2, "middle");

        int updatedSize = list.size();
        assertThat(updatedSize, is(initialSize + 3));
        assertFalse(list.isEmpty());
        assertThat(list.get(0), is("first"));
        assertThat(list.get(updatedSize - 1), is("last"));
        assertThat(list.get((updatedSize - 1) / 2), is("middle"));

        assertThat(list.remove(0), is("first"));
        assertThat(list.remove(list.size() - 1), is("last"));
        assertThat(list.remove((list.size() - 1) / 2), is("middle"));
        assertThat(list.size(), is(initialSize));
        assertFalse(list.isEmpty());
    }

    @Test
    void get() {
        for (int i = 0; i < tokens.length; i++) {
            assertThat(list.get(i), is(tokens[i]));
        }
    }

    @Test
    void set() {
        for (int i = 0, n = tokens.length; i < n; i++) {
            list.set(n - 1 - i, tokens[i]);
        }

        for (int i = 0, n = tokens.length; i < n; i++) {
            assertThat(list.get(n - 1 - i), is(tokens[i]));
        }

        list = new MyLinkedList<>(tokens);
    }
}