package datastructures.basic;

import datastructures.basic.impl.MyBinaryTreeImpl;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyBinaryTreeImplTest {

    private static final int NUMBER_OF_ELEMENTS = 1000;
    private static final int MAX_BOUND = 1000;


    @Test
    void operations() {
        Random rand = new Random();
        MyBinaryTree<Integer> mbt = new MyBinaryTreeImpl<>();

        assertTrue(mbt.isEmpty());

        Integer[] array = new Integer[NUMBER_OF_ELEMENTS];
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            Integer element = rand.nextInt(MAX_BOUND);
            array[i] = element;
            mbt.insert(element);
            assertTrue(mbt.contains(element));
        }

        assertThat(mbt.size(), is(NUMBER_OF_ELEMENTS));
        assertFalse(isSorted(array));
        Arrays.sort(array);
        assertTrue(isSorted(array));

        assertThat(mbt.getMinimum(), is(array[0]));
        assertThat(mbt.getMaximum(), is(array[array.length - 1]));

//        System.out.println(mbt.toString());

        Integer[] arrayFromBinaryTree = Arrays.stream(mbt.toString().trim()
                                                         .split("\\s+"))
                                              .map(Integer::parseInt)
                                              .collect(Collectors.toList())
                                              .toArray(array);

        assertTrue(isSorted(arrayFromBinaryTree));
    }

    private static <T extends Comparable<? super T>> boolean isSorted(T[] a) {
        for (int i = 1; i < a.length; i++) {
            if (a[i - 1].compareTo(a[i]) > 0) return false;
        }
        return true;
    }
}