package datastructures.basic;

import datastructures.basic.impl.MyPriorityQueueImpl;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static java.util.Objects.isNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyPriorityQueueImplTest {

    private static final int NUMBER_OF_ELEMENTS = 100;
    private static final int MAX_BOUND = 1000;


    @Test
    void operations() {
        Random rand = new Random();
        MyPriorityQueue<Integer> mpq = new MyPriorityQueueImpl<>(0);
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            mpq.add(rand.nextInt(MAX_BOUND));
        }

        while (!mpq.isEmpty()) {
            Integer maxElement = mpq.peek();
//            System.out.print(maxElement + " ");
            assertEquals(maxElement, mpq.poll());
            Integer nextMaxElement = mpq.peek();
            assertTrue(isNull(nextMaxElement) || maxElement >= nextMaxElement);
        }

        mpq = new MyPriorityQueueImpl<>(4);
        for (int i = 0; i < 10; i++) {
            mpq.add(rand.nextInt(MAX_BOUND));
        }

        mpq.clear();
        assertTrue(mpq.isEmpty());
    }
}