package datastructures.basic;

import datastructures.basic.impl.MyQueueImpl;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MyQueueImplTest {

    private String[] tokens = { "aa", "bb", "cc", "dd", "ee", "ff", "gg" };

    @Test
    void queueTest() {
        MyQueue<String> queue = new MyQueueImpl<>();
        assertTrue(queue.isEmpty());
        assertThrows(EmptyQueueException.class, queue::dequeue);
        assertThrows(EmptyQueueException.class, queue::peek);

        for (String token : tokens) {
            assertTrue(queue.enqueue(token));
            assertThat(queue.peek(), is(tokens[0]));
        }
        assertFalse(queue.isEmpty());


        MyQueue<String> queue2 = new MyQueueImpl<>();
        for (String token : tokens) {
            queue2.enqueue(token);
            assertThat(queue2.dequeue(), is(token));
        }
        assertTrue(queue2.isEmpty());
        assertThrows(EmptyQueueException.class, queue2::dequeue);
        assertThrows(EmptyQueueException.class, queue2::peek);

        for (String token : tokens) queue2.enqueue(token);

        for (String token : tokens) assertThat(queue2.dequeue(), is(token));
        assertTrue(queue2.isEmpty());
        assertThrows(EmptyQueueException.class, queue2::dequeue);
        assertThrows(EmptyQueueException.class, queue2::peek);

        for (String token : tokens) queue2.enqueue(token);
        for (String token : tokens) queue2.enqueue(token);

        for (String token : tokens) assertThat(queue2.dequeue(), is(token));
        assertFalse(queue2.isEmpty());
    }

}