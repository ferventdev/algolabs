package datastructures.basic.impl;

import datastructures.basic.MyList;


/**
 * This class represents the single linked list implementation of a list.
 * @param <E> - the type of elements held in this list.
 */
public class MyLinkedList<E> implements MyList<E> {

    private Node<E> head;
    private int size = 0;

    // This is a convenience constructor (may be easily used for tests)
    public MyLinkedList(E... items) {
        for (int i = items.length - 1; i >= 0; i--) {
            add(0, items[i]);
        }
    }

    private static class Node<E> {
        private E data;
        private Node<E> next;

        Node(E data, Node<E> next) {
            this.data = data;
            this.next = next;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(int index, E item) {
        if (index > size || index < 0) throw new IndexOutOfBoundsException("Index: " + index + ", size: " + size);
        Node<E> addedNode;
        if (index > 0) {
            Node<E> nodeBefore = getNode(index - 1);
            addedNode = new Node<>(item, nodeBefore.next);
            nodeBefore.next = addedNode;
        } else {
            addedNode = new Node<>(item, head);
            head = addedNode;
        }
        size++;
        return true;
    }

    @Override
    public E remove(int index) {
        checkIndex(index);
        E removedItem;
        Node<E> nodeToBeRemoved;
        if (index > 0) {
            Node<E> nodeBefore = getNode(index - 1);
            nodeToBeRemoved = nodeBefore.next;
            nodeBefore.next = nodeToBeRemoved.next;
        } else {
            nodeToBeRemoved = head;
            head = head.next;
        }
        removedItem = nodeToBeRemoved.data;
        nodeToBeRemoved.data = null;
        nodeToBeRemoved.next = null;
        size--;
        return removedItem;
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return getNode(index).data;
    }

    @Override
    public E set(int index, E item) {
        checkIndex(index);
        Node<E> node = getNode(index);
        E oldData = node.data;
        node.data = item;
        return oldData;
    }

    private void checkIndex(int index) {
        if (index >= size || index < 0) throw new IndexOutOfBoundsException("Index: " + index + ", size: " + size);
    }

    private Node<E> getNode(int index) {
        Node<E> node = head;
        for (int i = 0; i < index; i++)
            node = node.next;
        return node;
    }
}
