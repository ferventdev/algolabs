package datastructures.basic.impl;

import datastructures.basic.EmptyQueueException;
import datastructures.basic.MyQueue;
import datastructures.basic.MyStack;

/**
 * This class represents the queue implementation above two stacks.
 * @param <E> - the type of elements held in this queue.
 */
public class MyQueueImpl<E> implements MyQueue<E> {

    private final MyStack<E> input = new MyStackImpl<>();
    private final MyStack<E> output = new MyStackImpl<>();

    @Override
    public boolean isEmpty() {
        return output.isEmpty() && input.isEmpty();
    }

    @Override
    public boolean enqueue(E item) {
        return input.push(item);
    }

    @Override
    public E dequeue() {
        transfer();
        return output.pop();
    }

    @Override
    public E peek() {
        transfer();
        return output.peek();
    }

    private void transfer() {
        if (output.isEmpty()) {
            if (input.isEmpty()) throw new EmptyQueueException();
            do {
                output.push(input.pop());
            } while (!input.isEmpty());
        }
    }
}
