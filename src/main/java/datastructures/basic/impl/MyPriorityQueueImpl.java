package datastructures.basic.impl;

import datastructures.basic.MyPriorityQueue;

import java.util.Arrays;
import java.util.Comparator;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * This class represents the array based implementation of a priority queue.
 * @param <E> - the type of elements held in this queue.
 */
public class MyPriorityQueueImpl<E> implements MyPriorityQueue<E> {

    private static final int DEFAULT_INITIAL_CAPACITY = 8;

    private final int initialCapacity;
    private final Comparator<? super E> comparator;
    private E[] data;
    private int size;


    @SuppressWarnings("unchecked")
    public MyPriorityQueueImpl(int initialCapacity, Comparator<? super E> comparator) {
        if (initialCapacity < 0) throw new IllegalArgumentException("Initial capacity can't be negative");
        this.initialCapacity = initialCapacity;
        data = (E[]) new Object[initialCapacity];
        this.comparator = comparator;
    }

    public MyPriorityQueueImpl(int initialCapacity) {
        this(initialCapacity, null);
    }

    public MyPriorityQueueImpl() {
        this(DEFAULT_INITIAL_CAPACITY, null);
    }


    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void clear() {
        Arrays.fill(data, 0, size, null);
        data = (E[]) new Object[initialCapacity];
        size = 0;
    }

    @Override
    public E peek() {
        return isEmpty() ? null : data[0];
    }

    @Override
    public E poll() {
        if (isEmpty()) return null;
        E maxElement = data[0];
        data[0] = data[--size];
        data[size] = null;
        sink(0);
        return maxElement;
    }

    @Override
    public boolean add(E element) {
        if (isNull(element)) throw new IllegalArgumentException("Null elements are not permitted");
        if (size == data.length) ensureCapacity();
        data[size++] = element;
        swim(size - 1);
        return true;
    }

    @SuppressWarnings("unchecked")
    private void ensureCapacity() {
        E[] newData = (E[]) new Object[Math.max(DEFAULT_INITIAL_CAPACITY, data.length * 2)];
        System.arraycopy(data, 0, newData, 0, size);
        data = newData;
    }

    private static int parent(int index) {
        return (index + 1) / 2 - 1;
    }

    private static int leftChild(int index) {
        return 2 * (index + 1) - 1;
    }

    private static int rightChild(int index) {
        return 2 * (index + 1);
    }

    private void swim(int index) {
        int parent;
        while (index > 0 && less((parent = parent(index)), index)) {
            swap(parent, index);
            index = parent;
        }
    }

    private void sink(int index) {
        int left = leftChild(index);
        int right = rightChild(index);
        int largest = (left < size && less(index, left)) ? left : index;
        if (right < size && less(largest, right)) largest = right;
        if (largest == index) return;
        swap(index, largest);
        sink(largest);
    }

    @SuppressWarnings("unchecked")
    private boolean less(int index1, int index2) {
        if (nonNull(comparator)) return comparator.compare(data[index1], data[index2]) < 0;
        return ((Comparable<? super E>) data[index1]).compareTo(data[index2]) < 0;
    }

    private void swap(int i, int j) {
        E tmp = data[i];
        data[i] = data[j];
        data[j] = tmp;
    }
}
