package datastructures.basic.impl;

import datastructures.basic.MyBinaryTree;

import java.util.Comparator;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * This class represents a simple binary tree (without self-balancing).
 * @param <E> - the type of elements held in this tree.
 */
public class MyBinaryTreeImpl<E> implements MyBinaryTree<E> {

    private final Comparator<? super E> comparator;
    private Node<E> root;
    private int size;

    public MyBinaryTreeImpl(Comparator<? super E> comparator) {
        this.comparator = comparator;
    }

    public MyBinaryTreeImpl() {
        this.comparator = null;
    }


    private static class Node<E> {
        E key;
        Node<E> parent;
        Node<E> left;
        Node<E> right;

        Node(E key, Node<E> parent) {
            this.key = key;
            this.parent = parent;
        }

        public Node(E key) {
            this.key = key;
        }
    }


    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean contains(E key) {
        return nonNull(search(root, key));
    }

    private Node<E> search(Node<E> root, E key) {
        if (isNull(root) || equal(key, root.key)) return root;
        return search(less(key, root.key) ? root.left : root.right, key);
    }

    @Override
    public boolean insert(E keyToInsert) {
        if (isNull(keyToInsert)) throw new IllegalArgumentException("Null keys are not permitted in this binary tree");
        Node<E> parent = null;
        Node<E> insertionPlace = root;
        while (nonNull(insertionPlace)) {
            parent = insertionPlace;
            insertionPlace = less(keyToInsert, insertionPlace.key) ? insertionPlace.left : insertionPlace.right;
        }
        Node<E> nodeInserted = new Node<>(keyToInsert, parent);
        if (isNull(parent)) root = nodeInserted;
        else if (less(keyToInsert, parent.key)) parent.left = nodeInserted;
        else parent.right = nodeInserted;
        size++;
        return true;
    }

    @Override
    public E getMinimum() {
        Node<E> minimumNode = minimumNode(root);
        return isNull(minimumNode) ? null : minimumNode.key;
    }

    private Node<E> minimumNode(Node<E> node) {
        if (isNull(node)) return null;
        while (nonNull(node.left)) node = node.left;
        return node;
    }

    @Override
    public E getMaximum() {
        Node<E> maximumNode = maximumNode(root);
        return isNull(maximumNode) ? null : maximumNode.key;
    }

    private Node<E> maximumNode(Node<E> node) {
        if (isNull(node)) return null;
        while (nonNull(node.right)) node = node.right;
        return node;
    }

    @Override
    public String toString() {
        return toStringFromNode(root);
    }

    private String toStringFromNode(Node<E> node) {
        return isNull(node) ? "" :
                toStringFromNode(node.left) + " " +
                        node.key.toString() + " " +
                        toStringFromNode(node.right);
    }

    @SuppressWarnings("unchecked")
    private boolean less(E key1, E key2) {
        return nonNull(comparator) ?
                comparator.compare(key1, key2) < 0 :
                ((Comparable<? super E>) key1).compareTo(key2) < 0;
    }

    @SuppressWarnings("unchecked")
    private boolean equal(E key1, E key2) {
        return nonNull(comparator) ?
                comparator.compare(key1, key2) == 0 :
                ((Comparable<? super E>) key1).compareTo(key2) == 0;
    }
}
