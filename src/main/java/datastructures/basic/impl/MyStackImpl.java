package datastructures.basic.impl;

import datastructures.basic.MyList;
import datastructures.basic.MyStack;

import java.util.EmptyStackException;

/**
 * This class represents the stack implementation above a single linked list.
 * @param <E> - the type of elements held in this stack.
 */
public class MyStackImpl<E> implements MyStack<E> {

    private final MyList<E> stack = new MyLinkedList<>();

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }

    @Override
    public boolean push(E item) {
        return stack.add(0, item);
    }

    @Override
    public E pop() {
        if (stack.isEmpty()) throw new EmptyStackException();
        return stack.remove(0);
    }

    @Override
    public E peek() {
        if (stack.isEmpty()) throw new EmptyStackException();
        return stack.get(0);
    }

    public int size() {
        return stack.size();
    }
}