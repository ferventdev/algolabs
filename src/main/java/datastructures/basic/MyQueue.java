package datastructures.basic;

public interface MyQueue<E> {

    boolean isEmpty();

    boolean enqueue(E item);

    E dequeue();

    E peek();
}