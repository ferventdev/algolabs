package datastructures.basic;

public interface MyStack<E> {

    boolean isEmpty();

    boolean push(E item);

    E pop();

    E peek();
}