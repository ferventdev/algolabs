package datastructures.basic;

import datastructures.basic.impl.MyStackImpl;

public class BracketsBalance {

    private final static String BRACKETS = "()[]{}";

    public static boolean isBalanced(String str) {
        MyStack<Character> bracketsStack = new MyStackImpl<>();

        for (char symbol: str.toCharArray()) {
            int index = BRACKETS.indexOf(symbol);
            if (index == -1) continue;

            if (isOpeningBracket(index)) {
                bracketsStack.push(symbol);
            } else {
                if (bracketsStack.isEmpty() || !areCorresponding(bracketsStack.pop(), index)) return false;
            }
        }

        return bracketsStack.isEmpty();
    }

    private static boolean isOpeningBracket(int index) {
        return index % 2 == 0;
    }

    private static boolean areCorresponding(char opening, int closingIndex) {
        return BRACKETS.indexOf(opening) + 1 == closingIndex;
    }
}