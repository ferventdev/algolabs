package datastructures.basic;

public interface MyBinaryTree<E> {

    boolean isEmpty();

    int size();

    boolean contains(E e);

    boolean insert(E e);

    E getMinimum();

    E getMaximum();
}
