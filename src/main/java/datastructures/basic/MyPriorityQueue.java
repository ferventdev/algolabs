package datastructures.basic;

public interface MyPriorityQueue<E> {

    boolean isEmpty();

    int size();

    void clear();

    boolean add(E e);

    E peek();

    E poll();
}