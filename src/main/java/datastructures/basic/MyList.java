package datastructures.basic;

public interface MyList<E> {

    boolean isEmpty();

    int size();

    boolean add(int index, E item);

    E remove(int index);

    E get(int index);

    E set(int index, E item);
}