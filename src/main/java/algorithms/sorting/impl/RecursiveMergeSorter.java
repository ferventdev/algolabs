package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class RecursiveMergeSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        @SuppressWarnings("unchecked")
        T[] auxiliary = (T[]) new Object[data.length];
        sort(data, auxiliary,0,data.length - 1, cmpr);
    }

    private <T> void sort(T[] data, T[] aux, int left, int right, Comparator<? super T> cmpr) {
        if (right <= left) return;
        int mid = left + (right - left) / 2;
        sort(data, aux, left, mid, cmpr);
        sort(data, aux, mid + 1, right, cmpr);
        merge(data, aux, left, mid, right, cmpr);
    }

    private <T> void merge(T[] data, T[] aux, int left, int mid, int right, Comparator<? super T> cmpr) {
        System.arraycopy(data, left, aux, left, right - left + 1);
        for (int k = left, p = left, q = mid + 1; k <= right; k++) {
            if (q > right || p <= mid && cmpr.compare(aux[p], aux[q]) < 0) data[k] = aux[p++];
            else data[k] = aux[q++];
        }
    }
}