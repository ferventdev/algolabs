package algorithms.sorting.impl;

import java.util.Comparator;
import java.util.Random;

public class QuickWithInsertionSorter extends QuickSorter {

    private static final int CUTOFF = 10;
    private static final InsertionSorter insertionSorter = new InsertionSorter();

    private final Random random = new Random();


    private <T> void quicksort(T[] data, int p, int r, Comparator<? super T> cmpr) {
        if (p + CUTOFF >= r) {
            insertionSorter.sort(data, p, r, cmpr);
            return;
        }
        int q = partition(data, p, r, cmpr);
        quicksort(data, p, q - 1, cmpr);
        quicksort(data, q + 1, r, cmpr);
    }
}
