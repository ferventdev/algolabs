package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class BubbleSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        boolean swapped;
        do {
            swapped = false;
            for (int i = 0; i < data.length - 1; i++) {
                if (cmpr.compare(data[i], data[i + 1]) > 0) {
                    swap(data, i, i + 1);
                    swapped = true;
                }
            }
        } while (swapped);
    }
}