package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class SelectionSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        int N = data.length;
        for (int i = 0; i < N - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < N; j++) {
                if (cmpr.compare(data[j], data[minIndex]) < 0) minIndex = j;
            }
            swap(data, i, minIndex);
        }
    }
}