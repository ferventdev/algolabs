package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class HeapSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        buildMaxHeap(data, cmpr);
        for (int i = data.length - 1, heapSize = data.length; i >= 1; i--) {
            swap(data, i, 0);
            heapify(data, --heapSize, 0, cmpr);
        }
    }


    private <T> void buildMaxHeap(T[] data, Comparator<? super T> cmpr) {
        for (int i = data.length / 2 - 1; i >= 0; i--) heapify(data, data.length, i, cmpr);
    }

    private <T> void heapify(T[] data, int heapSize, int index, Comparator<? super T> cmpr) {
        int left = leftChild(index);
        int right = rightChild(index);
        int largest = (left < heapSize && cmpr.compare(data[index], data[left]) < 0) ? left : index;
        if (right < heapSize && cmpr.compare(data[largest], data[right]) < 0) largest = right;
        if (largest == index) return;
        swap(data, index, largest);
        heapify(data, heapSize, largest, cmpr);
    }


    private static int leftChild(int index) {
        return 2 * (index + 1) - 1;
    }

    private static int rightChild(int index) {
        return 2 * (index + 1);
    }
}