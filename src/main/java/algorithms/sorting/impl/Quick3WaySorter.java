package algorithms.sorting.impl;

import java.util.Comparator;

public class Quick3WaySorter extends QuickSorter {

    private <T> void quicksort(T[] data, int p, int r, Comparator<? super T> cmpr) {
        if (p >= r) return;

        int k = random.nextInt(r - p + 1) + p;
        swap(data, p, k);

        int lt = p, i = p + 1, gt = r;
        T v = data[p];

        while (i <= gt)
        {
            int cmp = cmpr.compare(data[i], v);
            if (cmp < 0) swap(data, lt++, i++);
            else if (cmp > 0) swap(data, i, gt--);
            else i++;
        } // Now data[lo..lt-1] < v = data[lt..gt] < data[gt+1..hi]

        quicksort(data, p, lt - 1, cmpr);
        quicksort(data, gt + 1, r, cmpr);
    }
}
