package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class IterativeMergeSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        @SuppressWarnings("unchecked")
        T[] auxiliary = (T[]) new Object[data.length];

        int N = data.length;
        for (int s = 1; s < N; s *= 2)
            for (int left = 0; left < N - s; left += 2 * s)
                merge(data, auxiliary, left, left + s - 1, Math.min(left + 2 * s - 1, N - 1), cmpr);
    }

    private <T> void merge(T[] data, T[] aux, int left, int mid, int right, Comparator<? super T> cmpr) {
        System.arraycopy(data, left, aux, left, right - left + 1);
        for (int k = left, p = left, q = mid + 1; k <= right; k++) {
            if (q > right || p <= mid && cmpr.compare(aux[p], aux[q]) < 0) data[k] = aux[p++];
            else data[k] = aux[q++];
        }
    }
}