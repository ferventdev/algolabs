package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;

import static java.util.Objects.isNull;

public class InsertionSorter implements Sorter {

    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        sort(data, 0, data.length - 1, cmpr);
    }

    protected <T> void sort(T[] data, int p, int r, Comparator<? super T> cmpr) {
        for (int i = p + 1; i <= r; i++) {
            T key = data[i];
            int j = i - 1;
            for ( ; j >= 0 && cmpr.compare(data[j], key) > 0; j--) data[j + 1] = data[j];
            data[j + 1] = key;
        }
    }
}