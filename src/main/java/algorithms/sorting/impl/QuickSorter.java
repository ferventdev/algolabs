package algorithms.sorting.impl;

import algorithms.sorting.Sorter;

import java.util.Comparator;
import java.util.Random;

import static java.util.Objects.isNull;

public class QuickSorter implements Sorter {

    protected final Random random = new Random();


    @Override
    public <T> void sort(T[] data, Comparator<? super T> cmpr) {
        if (isNull(data) || isNull(cmpr)) throw new IllegalArgumentException();

        quicksort(data, 0, data.length - 1, cmpr);
    }

    private <T> void quicksort(T[] data, int p, int r, Comparator<? super T> cmpr) {
        if (p >= r) return;
        int q = partition(data, p, r, cmpr);
        quicksort(data, p, q - 1, cmpr);
        quicksort(data, q + 1, r, cmpr);
    }

    protected <T> int partition(T[] data, int p, int r, Comparator<? super T> cmpr) {
        int k = random.nextInt(r - p + 1) + p;
        swap(data, r, k);
        T last = data[r];
        int i = p - 1;
        for (int j = p; j < r; j++) {
            if (cmpr.compare(data[j], last) <= 0) {
                i++;
                swap(data, i, j);
            }
        }
        swap(data, i + 1, r);
        return i + 1;
    }
}
