package algorithms.sorting;

import java.util.Comparator;

import static java.util.Objects.isNull;

/**
 * This interface is made just in order not to duplicate some code in some sorters.
 */
public interface Sorter {

    <T> void sort(T[] a, Comparator<? super T> comparator);

    default <T extends Comparable<? super T>> void sort(T[] data) {
        sort(data, Comparator.naturalOrder());
    }

    default <T> void swap(T[] data, int i, int j) {
        T tmp = data[i];
        data[i] = data[j];
        data[j] = tmp;
    }
}